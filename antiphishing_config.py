"""Antiphishing config reader
"""
from os import path
import yaml
try:
    from gglsbl import SafeBrowsingList as sbl  # type: ignore
except ImportError:
    sbl = None


redis_conf = {'host': 'localhost', 'port': 6379, 'decode_responses': True}
config = {}
config['redis'] = redis_conf
SafeBrowsingList = sbl

if path.exists('/etc/antiphishing-service.yml'):
    with open('/etc/antiphishing-service.yml',
              mode='r',
              encoding='utf-8') as file:
        config = yaml.safe_load(file)
        file.close()
    if 'redis' in config:
        config['redis'] = redis_conf | config['redis']
    else:
        config['redis'] = redis_conf
