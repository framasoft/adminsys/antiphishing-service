#!/usr/bin/env python3
"""Antiphishing database populator

To lint the file, install python3-pycodestyle, pylint, mypy, then:
    python /usr/lib/python3/dist-packages/pycodestyle.py \
        populate_database.py
    pylint --rcfile .pylintrc populate_database.py
    mypy populate_database.py
"""

import argparse
import csv
import re
import tarfile
from datetime import datetime
from io import BytesIO
from pprint import pformat

import redis
import requests
from antiphishing_config import config, SafeBrowsingList

r = redis.Redis(**config['redis'])  # type: ignore

HEADERS = {'User-Agent': 'Framasoft antiphishing service'}

if 'exclusions' in config:
    exclusions = re.compile(
                    f"({'|'.join(config['exclusions'])})")


def insert_or_update(url: str, source: str) -> bool:
    """Insert or update url in the database

    Keyword arguments:
    url    -- the url to insert
    source -- the source of insertion or update
    """
    if exclusions is not None:
        if exclusions.match(url):
            return False
    record = r.hgetall(url)
    if len(record) == 0:
        r.hset(url, mapping={
            source: str(datetime.now())})
    else:
        record[source] = str(datetime.now())
        r.hset(url, mapping=record)

    return True


def get_phishstats() -> bool:
    """https://phishstats.info/phish_score.csv

    Updated every 90 minutes with phishing URLs from the past 30 days.
    """
    print('Getting URLs from Phishstats')
    try:
        req = requests.get('https://phishstats.info/phish_score.csv',
                           headers=HEADERS,
                           timeout=60)
    except requests.exceptions.RequestException as req_err:
        print(f'{pformat(req_err, indent=2, sort_dicts=False)}')
        return False

    if req.status_code != 200:
        print(f'HTTPError: {req.status_code} {req.reason}')
        return False

    phishstats = csv.reader(req.content.decode('utf-8').splitlines(),
                            dialect='unix')
    comment = True
    for row in phishstats:
        if comment:
            if not row[0].startswith('#'):
                comment = False
        else:
            if float(row[1]) > 4:
                insert_or_update(row[2], 'phishstats')
    return True


def get_phishtank() -> bool:
    """https://data.phishtank.com/data/online-valid.json

    Updated hourly
    """
    print('Getting URLs from Phishtank')
    try:
        req = requests.get('https://data.phishtank.com/data/online-valid.json',
                           headers=HEADERS,
                           timeout=60)
    except requests.exceptions.RequestException as req_err:
        print(f'{pformat(req_err, indent=2, sort_dicts=False)}')
        return False

    if req.status_code != 200:
        print(f'HTTPError: {req.status_code} {req.reason}')
        return False

    phishtank = req.json()
    for row in phishtank:
        if row['verified'] == 'yes':
            insert_or_update(row['url'], 'phishtank')
    return True


def get_phishing_database() -> bool:
    """https://raw.githubusercontent.com/mitchellkrogza/
               Phishing.Database/master/ALL-phishing-links.tar.gz

    Updated every 2 hours
    """
    print('Getting URLs from Phishing database')
    try:
        req = requests.get('https://raw.githubusercontent.com/mitchellkrogza/\
Phishing.Database/master/ALL-phishing-links.tar.gz',
                           headers=HEADERS,
                           timeout=60)
    except requests.exceptions.RequestException as req_err:
        print(f'{pformat(req_err, indent=2, sort_dicts=False)}')
        return False

    if req.status_code != 200:
        print(f'HTTPError: {req.status_code} {req.reason}')
        return False

    with tarfile.open(name=None, mode='r:gz', fileobj=BytesIO(req.content)) \
         as tar:
        for member in tar.getmembers():
            phishingdb = tar.extractfile(member)
            if phishingdb:
                content = phishingdb.read()
                for row in content.split(b'\n'):
                    url = row.decode('utf-8')
                    if len(url) > 0:
                        insert_or_update(url, 'phishingdb')
    return True


def get_phishunt() -> bool:
    """https://phishunt.io/feed.txt

    Updated every 2 hours
    """
    print('Getting URLs from Phishunt database')
    try:
        req = requests.get('https://phishunt.io/feed.txt',
                           headers=HEADERS,
                           timeout=60)
    except requests.exceptions.RequestException as req_err:
        print(f'{pformat(req_err, indent=2, sort_dicts=False)}')
        return False

    if req.status_code != 200:
        print(f'HTTPError: {req.status_code} {req.reason}')
        return False

    for row in req.content.decode('utf-8').splitlines():
        insert_or_update(row, 'phishunt')
    return True


def refresh_gsb() -> bool:
    """Sync local hash prefix cache
    """
    if SafeBrowsingList is None:
        print('Please install gglsbl python module')
        return False
    if 'gsb_key' not in config:
        print('Please add gsb_key in the configuration file')
        return False

    sbl = SafeBrowsingList(config['gsb_key'],
                           'google-safe-browsing.db')
    sbl.update_hash_prefix_cache()
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--flushdb',
                        dest='flushdb',
                        help='Flush Redis DB before fetching domains',
                        action='store_true')
    parser.add_argument('-s', '--phishstats',
                        dest='phishstats',
                        help='Fetch domains from https://phishstats.info',
                        action='store_true')
    parser.add_argument('-t', '--phishtank',
                        dest='phishtank',
                        help='Fetch domains from https://data.phishtank.com \
(usually don’t work)',
                        action='store_true')
    parser.add_argument('-d', '--phishingdb',
                        dest='phishingdb',
                        help='Fetch domains from https://github.com/\
mitchellkrogza/Phishing.Database',
                        action='store_true')
    parser.add_argument('-p', '--phishunt',
                        dest='phishunt',
                        help='Fetch domains from https://phishunt.io/',
                        action='store_true')
    parser.add_argument('-g', '--google-safe-browsing',
                        dest='gsb',
                        help='Refresh the local cache of Google Safe \
Browsing hash prefix cache',
                        action='store_true')
    parser.add_argument('-a', '--all',
                        dest='all',
                        help='Fetch domains from all sources',
                        action='store_true')

    args = parser.parse_args()

    if args.flushdb:
        print('Flushing redis database')
        r.flushdb()

    if args.phishstats or args.all:
        get_phishstats()

    if args.phishtank or args.all:
        get_phishtank()

    if args.phishingdb or args.all:
        get_phishing_database()

    if args.phishunt or args.all:
        get_phishunt()

    if args.gsb or args.all:
        refresh_gsb()
