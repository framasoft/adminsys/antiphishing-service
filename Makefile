.PHONY: lint-config lint-flask lint-db-populator lint

all: lint

lint-config: antiphishing_config.py
	python /usr/lib/python3/dist-packages/pycodestyle.py antiphishing_config.py
	pylint --rcfile .pylintrc antiphishing_config.py
	mypy antiphishing_config.py

lint-flask: antiphishing_service.py
	python /usr/lib/python3/dist-packages/pycodestyle.py antiphishing_service.py
	pylint --rcfile .pylintrc antiphishing_service.py
	mypy antiphishing_service.py

lint-db-populator: populate_database.py
	python /usr/lib/python3/dist-packages/pycodestyle.py populate_database.py
	pylint --rcfile .pylintrc populate_database.py
	mypy populate_database.py

lint: antiphishing_service.py populate_database.py
	python /usr/lib/python3/dist-packages/pycodestyle.py antiphishing_config.py antiphishing_service.py populate_database.py
	pylint --rcfile .pylintrc antiphishing_config.py antiphishing_service.py populate_database.py
	mypy antiphishing_config.py antiphishing_service.py populate_database.py
